/*
Navicat MySQL Data Transfer

Source Server         : 拓道机房
Source Server Version : 50725
Source Host           : 192.168.100.4:3306
Source Database       : zhisheng

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-12-13 10:57:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for zs_sys_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `zs_sys_auth_group`;
CREATE TABLE `zs_sys_auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id',
  `nums` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色人数',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '新增时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_title` (`title`) USING BTREE COMMENT '角色名唯一'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of zs_sys_auth_group
-- ----------------------------
INSERT INTO `zs_sys_auth_group` VALUES ('1', '系统管理员', '1', '2,10,41,42,43,44,45,11,46,47,48,49,50,3,12,51,52,53,54,55,56,57,58,79,13,101,93,94,95,96,97,98,99,80,14,102,81,82,83,84,85,86,87,88,15,59,60,61,62,4,16,63,64,65,66,17,67,68,69,18,70,89,90,92,19,71,106,100,117,107,108,109,111,112,113,114,115,5,20,28,22,72,73,21,74,75,76,77,1,6,23,25,26,27,29,30,110,7,24,33,34,35,118,8,36,37,9,38,39,40,103,119,31,32,116,104,105,78', '1', '2019-10-10 10:47:57', '2019-12-13 10:36:44');
INSERT INTO `zs_sys_auth_group` VALUES ('2', '审核人员', '0', '1,6,23,25,26,27,29,30,110,7,24,33,34,35,118,8,36,37,9,38,39,40,103,119,2,10,41,42,43,44,45,11,46,47,48,49,50,31,32,116,104,105', '3', '2019-10-17 10:51:28', '2019-12-13 10:45:52');
INSERT INTO `zs_sys_auth_group` VALUES ('3', '测试人员', '1', '2,10,41,42,43,44,45,11,46,47,48,49,50,3,12,51,52,53,54,55,56,57,58,79,13,101,93,94,95,96,97,98,99,80,14,102,81,82,83,84,85,86,87,88,15,59,60,61,62,4,16,63,64,65,66,17,67,68,69,18,70,89,90,92,19,71,106,100,117,107,108,109,111,112,113,114,115,5,20,28,22,72,73,21,74,75,76,77,1,6,23,25,26,27,29,30,110,7,24,33,34,35,118,8,36,37,9,38,39,40,103,119,31,32,116,104,105,78', '1', '2019-10-17 11:42:10', '2019-12-13 10:36:33');
INSERT INTO `zs_sys_auth_group` VALUES ('4', '编辑审核', '1', '2,10,41,42,43,44,45,11,46,47,48,49,50,3,12,51,52,53,54,55,56,57,58,79,13,101,93,94,95,96,97,98,99,80,14,102,81,82,83,84,85,86,87,88,15,59,60,61,62,4,16,63,64,65,66,17,67,68,69,18,70,89,90,92,19,71,106,100,117,107,108,109,111,112,113,114,115,5,20,28,22,72,73,21,74,75,76,77,1,6,23,25,26,27,29,30,110,7,24,33,34,35,118,8,36,37,9,38,39,40,103,119,31,32,116,104,105,78', '1', '2019-12-12 16:30:06', '2019-12-13 10:36:38');
