/*
Navicat MySQL Data Transfer

Source Server         : 拓道机房
Source Server Version : 50725
Source Host           : 192.168.100.4:3306
Source Database       : zhisheng

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-12-13 10:58:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for zs_sys_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `zs_sys_auth_rule`;
CREATE TABLE `zs_sys_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1:正常0冻结',
  `add_condition` char(100) NOT NULL DEFAULT '',
  `listorder` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父类id',
  `is_nav` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否导航1导航，2导航+url 0url',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1公共权限，只要父级有权限公共的默认自动有权限',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `idx_public_listorder_nav_parent` (`is_public`,`listorder`,`is_nav`,`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4 COMMENT='节点表';

-- ----------------------------
-- Records of zs_sys_auth_rule
-- ----------------------------
INSERT INTO `zs_sys_auth_rule` VALUES ('1', 'content', '内容管理', '1', '', '1', '0', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('2', 'trade', '运营管理', '1', '', '2', '0', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('3', 'material', '素材管理', '1', '', '3', '0', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('4', 'users', '账号管理', '1', '', '4', '0', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('5', 'system', '系统管理', '1', '', '6', '0', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('6', 'content_task', '任务管理', '1', '', '6', '1', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('7', 'content_auth', '审核管理', '1', '', '7', '1', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('8', 'content_amount', '分账规则', '1', '', '8', '1', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('9', 'content_grade', '内容评级', '1', '', '9', '1', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('10', 'trade_bootad', '开机广告', '1', '', '10', '2', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('11', 'trade_layerad', '浮层广告', '1', '', '11', '2', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('12', 'material_pattern', '图案', '1', '', '12', '3', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('13', 'material_music', '音乐', '1', '', '13', '3', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('14', 'material_sound', '音效', '1', '', '14', '3', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('15', 'material_label', '标签', '1', '', '15', '3', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('16', 'users_cert', '认证审核', '1', '', '16', '4', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('17', 'users_copr', '版权审核', '1', '', '17', '4', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('18', 'users_account', '账号详情', '1', '', '18', '4', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('19', 'users_amount', '账户流水', '1', '', '19', '4', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('20', 'system_users', '人员管理', '1', '', '20', '5', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('21', 'system_role', '角色管理', '1', '', '21', '5', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('22', 'system_users.edit', '人员编辑/新增', '1', '', '2', '20', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('23', 'content_task.index', '任务列表', '1', '', '1', '6', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('24', 'content_auth.index', '审核列表', '1', '', '1', '7', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('25', 'content_task.edit', '任务新增/编辑', '1', '', '2', '6', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('26', 'content_task.info', '任务详情', '1', '', '3', '6', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('27', 'content_task.status', '任务状态', '1', '', '4', '6', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('28', 'system_users.index', '人员列表', '1', '', '1', '20', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('29', 'content_task.del', '任务删除', '1', '', '5', '6', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('30', 'content_task.sort', '任务排序', '1', '', '6', '6', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('31', 'content_task.genre', '所属分类', '1', '', '8', '6', '0', '1');
INSERT INTO `zs_sys_auth_rule` VALUES ('32', 'content_task.label', '标签列表', '1', '', '9', '6', '0', '1');
INSERT INTO `zs_sys_auth_rule` VALUES ('33', 'content_auth.edit', '作品编辑', '1', '', '2', '7', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('34', 'content_auth.status', '审核操作', '1', '', '3', '7', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('35', 'content_auth.del', '作品删除', '1', '', '4', '7', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('36', 'content_amount.lists', '分账列表', '1', '', '1', '8', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('37', 'content_amount.edit', '分账编辑', '1', '', '2', '8', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('38', 'content_grade.index', '评级列表', '1', '', '1', '9', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('39', 'content_grade.accept', '采纳操作', '1', '', '2', '9', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('40', 'content_grade.edit', '作品编辑', '1', '', '3', '9', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('41', 'trade_bootad.index', '列表', '1', '', '1', '10', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('42', 'trade_bootad.info', '详情', '1', '', '2', '10', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('43', 'trade_bootad.edit', '新增/编辑', '1', '', '3', '10', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('44', 'trade_bootad.status', '状态', '1', '', '4', '10', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('45', 'trade_bootad.del', '删除', '1', '', '5', '10', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('46', 'trade_layerad.index', '列表', '1', '', '1', '11', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('47', 'trade_layerad.info', '详情', '1', '', '2', '11', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('48', 'trade_layerad.edit', '新增/编辑', '1', '', '3', '11', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('49', 'trade_layerad.status', '状态', '1', '', '4', '11', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('50', 'trade_layerad.del', '删除', '1', '', '5', '11', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('51', 'material_pattern.index', '列表', '1', '', '1', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('52', 'material_pattern.info', '详情', '1', '', '2', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('53', 'material_pattern.edit', '新增/编辑', '1', '', '3', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('54', 'material_pattern.status', '启用/停用', '1', '', '4', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('55', 'material_pattern.sort', '排序', '1', '', '5', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('56', 'material_pattern.resource_edit', '素材新增/编辑', '1', '', '6', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('57', 'material_pattern.resource_status', '素材状态', '1', '', '7', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('58', 'material_pattern.resource_sort', '素材排序', '1', '', '8', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('59', 'material_label.index', '列表', '1', '', '1', '15', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('60', 'material_label.edit', '新增/编辑', '1', '', '2', '15', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('61', 'material_label.status', '状态操作', '1', '', '3', '15', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('62', 'material_label.del', '删除', '1', '', '4', '15', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('63', 'users_cert.index', '列表', '1', '', '1', '16', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('64', 'users_cert.status', '审核状态', '1', '', '2', '16', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('65', 'users_cert.del', '删除', '1', '', '3', '16', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('66', 'users_cert.info', '详情', '1', '', '4', '16', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('67', 'users_copr.index', '列表', '1', '', '1', '17', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('68', 'users_copr.status', '审核状态', '1', '', '2', '17', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('69', 'users_copr.del', '删除', '1', '', '3', '17', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('70', 'users_account.index', '列表', '1', '', '1', '18', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('71', 'users_amount.index', '列表', '1', '', '1', '19', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('72', 'system_users.status', '人员状态', '1', '', '3', '20', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('73', 'system_users.del', '人员删除', '1', '', '4', '20', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('74', 'system_role.index', '角色列表', '1', '', '1', '21', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('75', 'system_role.edit', '角色新增/编辑', '1', '', '2', '21', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('76', 'system_role.status', '角色状态', '1', '', '3', '21', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('77', 'system_role.authorize', '角色授权', '1', '', '4', '21', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('78', 'system_role._roles', '角色查询选择列表', '1', '', '5', '28', '0', '1');
INSERT INTO `zs_sys_auth_rule` VALUES ('79', 'material_pattern.publish', '发布', '1', '', '9', '12', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('80', 'material_music.publish', '发布', '1', '', '9', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('81', 'material_sound.info', '详情', '1', '', '2', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('82', 'material_sound.edit', '新增/编辑', '1', '', '3', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('83', 'material_sound.status', '启用/停用', '1', '', '4', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('84', 'material_sound.sort', '排序', '1', '', '5', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('85', 'material_sound.resource_edit', '素材新增/编辑', '1', '', '6', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('86', 'material_sound.resource_status', '素材状态', '1', '', '7', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('87', 'material_sound.resource_sort', '素材排序', '1', '', '8', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('88', 'material_sound.publish', '发布', '1', '', '9', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('89', 'users_account.status', '冻结', '1', '', '2', '18', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('90', 'users_account.info', '详情', '1', '', '3', '18', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('111', 'data', '数据管理', '1', '', '5', '0', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('92', 'users_account.payment', '用户打款状态', '1', '', '5', '18', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('93', 'material_music.info', '详情', '1', '', '2', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('94', 'material_music.edit', '新增/编辑', '1', '', '3', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('95', 'material_music.status', '启用/停用', '1', '', '4', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('96', 'material_music.sort', '排序', '1', '', '5', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('97', 'material_music.resource_edit', '素材新增/编辑', '1', '', '6', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('98', 'material_music.resource_status', '素材状态', '1', '', '7', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('99', 'material_music.resource_sort', '素材排序', '1', '', '8', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('100', 'users_amount.payment_make', '打款标记操作', '1', '', '3', '19', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('101', 'material_music.index', '列表', '1', '', '1', '13', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('102', 'material_sound.index', '列表', '1', '', '1', '14', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('103', 'content_grade.set_media_bank', '推送媒资库', '1', '', '4', '9', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('104', 'content_grade.task_lists', '任务列表', '1', '', '6', '9', '0', '1');
INSERT INTO `zs_sys_auth_rule` VALUES ('105', 'content_grade.info', '作品详情', '1', '', '7', '9', '0', '1');
INSERT INTO `zs_sys_auth_rule` VALUES ('106', 'users_amount.payment', '打款任务列表', '1', '', '2', '19', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('107', 'users_feedback', '用户反馈', '1', '', '20', '4', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('108', 'users_feedback.index', '列表', '1', '', '1', '107', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('109', 'users_feedback.del', '删除', '1', '', '2', '107', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('110', 'content_task.top', '轮播设置', '1', '', '7', '6', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('112', 'data_userstats', '用户统计', '1', '', '1', '111', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('113', 'data_userstats.index', '统计内容', '1', '', '1', '112', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('114', 'data_taskstats', '任务统计', '1', '', '2', '111', '1', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('115', 'data_taskstats.index', '统计内容', '1', '', '2', '114', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('116', 'content_auth.info', '作品详情', '1', '', '5', '7', '0', '1');
INSERT INTO `zs_sys_auth_rule` VALUES ('117', 'users_amount.export', '导出', '1', '', '4', '19', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('118', 'content_auth.export', '作品导出', '1', '', '6', '7', '0', '0');
INSERT INTO `zs_sys_auth_rule` VALUES ('119', 'content_grade.export', '作品导出', '1', '', '8', '9', '0', '0');
