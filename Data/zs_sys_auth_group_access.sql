/*
Navicat MySQL Data Transfer

Source Server         : 拓道机房
Source Server Version : 50725
Source Host           : 192.168.100.4:3306
Source Database       : zhisheng

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-12-13 10:57:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for zs_sys_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `zs_sys_auth_group_access`;
CREATE TABLE `zs_sys_auth_group_access` (
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `group_id` int(11) unsigned NOT NULL COMMENT '用户组id',
  PRIMARY KEY (`uid`,`group_id`),
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE,
  CONSTRAINT `zs_sys_auth_group_access_group_id` FOREIGN KEY (`group_id`) REFERENCES `zs_sys_auth_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zs_sys_auth_group_access_uid` FOREIGN KEY (`uid`) REFERENCES `zs_sys_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户组明细表';

-- ----------------------------
-- Records of zs_sys_auth_group_access
-- ----------------------------
INSERT INTO `zs_sys_auth_group_access` VALUES ('1', '1');
INSERT INTO `zs_sys_auth_group_access` VALUES ('2', '2');
INSERT INTO `zs_sys_auth_group_access` VALUES ('3', '3');
INSERT INTO `zs_sys_auth_group_access` VALUES ('4', '2');
INSERT INTO `zs_sys_auth_group_access` VALUES ('12', '2');
INSERT INTO `zs_sys_auth_group_access` VALUES ('13', '4');
