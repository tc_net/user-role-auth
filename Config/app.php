<?php

return array(
    //请将以下配置拷贝到 ./Config/app.php 文件对应的位置中，未配置的表将使用默认路由
    'auth' => array(
        'auth_on'           => true, // 认证开关
        'auth_user'         => 'sys_users', // 用户信息表,
        'auth_group'        => 'sys_auth_group', // 组数据表名
        'auth_group_access' => 'sys_auth_group_access', // 用户-组关系表
        'auth_rule'         => 'sys_auth_rule', // 权限规则表
        'auth_not_check_user'=> array(), //跳过权限检测的用户
        'auth_not_check_node'=> array('home','upload','cgi_captcha','cgi_notify')  //跳过权限检测的节点
    )
);


